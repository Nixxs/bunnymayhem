﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BunnyMayhem
{
    class BodyData
    {
        private Type _sprite_type;
        private object _parent_object;

        public BodyData (Type sprite_type, object parent_object)
        {
            _sprite_type = sprite_type;
            _parent_object = parent_object;
        }

        public enum Type
        {
            PLAYER,
            FLOOR,
            CIELING,
            WALL,
            CORPSE,
            POWERUP
        }

        public Type SpriteType
        {
            get { return _sprite_type; }
        }

        public object ParentObject
        {
            get { return _parent_object; }
        }
    }
}
