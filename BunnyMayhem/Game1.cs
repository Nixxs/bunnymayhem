﻿using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System.Collections.Generic;

namespace BunnyMayhem
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        World world;
        float units_ratio;
        List<SoundEffect> soundEffects;
        SpriteFont font;

        List<Platform> platforms;
        List<Player> players;
        List<PowerUp> powerups;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferHeight = 720;

            Content.RootDirectory = "Content";
            world = new World(new Vector2(0,9.82f));
            units_ratio = 64.0f;
            platforms = new List<Platform>();
            players = new List<Player>();
            powerups = new List<PowerUp>();
            soundEffects = new List<SoundEffect>();
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            // load the font
            font = Content.Load<SpriteFont>("MonoGameFont");


            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            ConvertUnits.SetDisplayUnitToSimUnitRatio(64.0f);
            Texture2D grassTexture = Content.Load<Texture2D>("grassblock");

            // Create boundaries out of platform objects
            int ground_width = graphics.GraphicsDevice.Viewport.Width;
            int ground_height = 25;
            Platform ground = new Platform
            (
                graphics.GraphicsDevice, 
                world,
                grassTexture,
                ground_width, 
                ground_height, 
                Color.Chartreuse, 
                new Vector2(ground_width/2f, graphics.GraphicsDevice.Viewport.Height - (ground_height/2f)), 
                units_ratio,
                BodyData.Type.FLOOR
            );
            platforms.Add(ground);

            int wall_height = graphics.GraphicsDevice.Viewport.Height * 2;
            int wall_width = 10;
            Platform left_wall = new Platform
            (
                graphics.GraphicsDevice,
                world,
                grassTexture,
                wall_width,
                wall_height,
                Color.Red,
                new Vector2((-wall_width/2f), 0),
                units_ratio,
                BodyData.Type.WALL
            );
            platforms.Add(left_wall);

            Platform right_wall = new Platform
            (
                graphics.GraphicsDevice,
                world,
                grassTexture,
                wall_width,
                wall_height,
                Color.Red,
                new Vector2(graphics.GraphicsDevice.Viewport.Width + (wall_width/2f), 0),
                units_ratio,
                BodyData.Type.WALL
            );
            platforms.Add(right_wall);

            int ceiling_height = 10;
            Platform ceiling = new Platform
            (
                graphics.GraphicsDevice,
                world,
                grassTexture,
                graphics.GraphicsDevice.Viewport.Width,
                ceiling_height,
                Color.Red,
                new Vector2(graphics.GraphicsDevice.Viewport.Width / 2f, -graphics.GraphicsDevice.Viewport.Height),
                units_ratio,
                BodyData.Type.CIELING
            );
            platforms.Add(ceiling);

            // world platforms
            int mid_left_platform_width = graphics.GraphicsDevice.Viewport.Width / 7;
            int mid_left_platform_height = 25;
            Platform mid_left_platform = new Platform
            (
                graphics.GraphicsDevice,
                world,
                grassTexture,
                mid_left_platform_width,
                mid_left_platform_height,
                Color.Chartreuse,
                new Vector2(mid_left_platform_width / 2, graphics.GraphicsDevice.Viewport.Height / 1.4f),
                units_ratio,
                BodyData.Type.FLOOR
            );
            platforms.Add(mid_left_platform);

            int mid_right_platform_width = graphics.GraphicsDevice.Viewport.Width / 7;
            int mid_right_platform_height = 25;
            Platform mid_right_platform = new Platform
            (
                graphics.GraphicsDevice,
                world,
                grassTexture,
                mid_right_platform_width,
                mid_right_platform_height,
                Color.Chartreuse,
                new Vector2(graphics.GraphicsDevice.Viewport.Width - (mid_right_platform_width / 2), graphics.GraphicsDevice.Viewport.Height / 1.4f),
                units_ratio,
                BodyData.Type.FLOOR
            );
            platforms.Add(mid_right_platform);

            int top_platforms_width = graphics.GraphicsDevice.Viewport.Width / 7;
            Platform top_left_platform = new Platform
            (
                graphics.GraphicsDevice,
                world,
                grassTexture,
                top_platforms_width,
                mid_left_platform_height,
                Color.Chartreuse,
                new Vector2(top_platforms_width / 2, graphics.GraphicsDevice.Viewport.Height / 4f),
                units_ratio,
                BodyData.Type.FLOOR
            );
            platforms.Add(top_left_platform);

            Platform top_right_platform = new Platform
            (
                graphics.GraphicsDevice,
                world,
                grassTexture,
                top_platforms_width,
                mid_right_platform_height,
                Color.Chartreuse,
                new Vector2(graphics.GraphicsDevice.Viewport.Width - (top_platforms_width / 2), graphics.GraphicsDevice.Viewport.Height / 4f),
                units_ratio,
                BodyData.Type.FLOOR
            );
            platforms.Add(top_right_platform);

            int mid_platform_width = graphics.GraphicsDevice.Viewport.Width / 4;
            int mid_platform_height = 25;
            Platform mid_platform = new Platform
            (
                graphics.GraphicsDevice,
                world,
                grassTexture,
                mid_platform_width,
                mid_platform_height,
                Color.Chartreuse,
                new Vector2((graphics.GraphicsDevice.Viewport.Width / 2f), graphics.GraphicsDevice.Viewport.Height / 2f),
                units_ratio,
                BodyData.Type.FLOOR
            );
            platforms.Add(mid_platform);

            // create players
            Texture2D bunny_sheet = Content.Load<Texture2D>("BunnySpriteSheet");
            float player_spawn_height = -graphics.GraphicsDevice.Viewport.Height + (graphics.GraphicsDevice.Viewport.Height/2);
            soundEffects.Add(Content.Load<SoundEffect>("Splat_1"));
            soundEffects.Add(Content.Load<SoundEffect>("jump"));
            soundEffects.Add(Content.Load<SoundEffect>("woosh"));
            soundEffects.Add(Content.Load<SoundEffect>("Squish_2"));

            Player player_one = new Player
            (
                graphics.GraphicsDevice,
                soundEffects,
                world,
                bunny_sheet,
                42,
                42,
                new Vector2((graphics.GraphicsDevice.Viewport.Width / 1.5f), player_spawn_height),
                Color.White,
                units_ratio,
                Keys.Left,
                Keys.Right,
                Keys.Up,
                Keys.Down
            );
            players.Add(player_one);

            Player player_two = new Player
            (
                graphics.GraphicsDevice,
                soundEffects,
                world,
                bunny_sheet,
                42,
                42,
                new Vector2((graphics.GraphicsDevice.Viewport.Width / 3.5f), player_spawn_height),
                Color.BurlyWood,
                units_ratio,
                Keys.A,
                Keys.D,
                Keys.W,
                Keys.S
            );
            players.Add(player_two);

            Player player_three = new Player
            (
                graphics.GraphicsDevice,
                soundEffects,
                world,
                bunny_sheet,
                42,
                42,
                new Vector2((graphics.GraphicsDevice.Viewport.Width/2), player_spawn_height),
                Color.Gray,
                units_ratio,
                Keys.J,
                Keys.L,
                Keys.I,
                Keys.K
            );
            players.Add(player_three);

            PowerUp carrot = new PowerUp
            (
                world,
                Content.Load<Texture2D>("Carrot"),
                new Vector2(((graphics.GraphicsDevice.Viewport.Width / 2) + 100), player_spawn_height + 30),
                units_ratio
            );
            powerups.Add(carrot);
        }

        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        protected override void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                Exit();
            }

            foreach (Player player in players)
            {
                player.Update(gameTime);
            }

            world.Step((float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.001f);

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            // draw all the players and thier kill counts
            // kill count row is just so each players kills is drawn down the screen
            float kill_count_row = 1f;
            foreach (Player player in players)
            {
                player.Draw(spriteBatch);
                float row_height = (12f * kill_count_row);
                Vector2 text_position = new Vector2(32f, row_height);
                spriteBatch.DrawString(font, player.Info.Kills.ToString(), text_position, Color.Black, 0f, new Vector2(0, 0), 1.1f, SpriteEffects.None, 1.0f);
                spriteBatch.DrawString(font, player.Info.Kills.ToString(), text_position, Color.White, 0f, new Vector2(0, 0), 1f, SpriteEffects.None, 1.0f);
                spriteBatch.Draw(player.Info.Icon, new Vector2(15f, row_height + 7f), player.Info.IconSourceRect, player.Info.IconColor, 0f, new Vector2(player.Info.IconSourceRect.Width / 2f, player.Info.IconSourceRect.Height / 2f), 0.55f, SpriteEffects.None, 1.0f);
                kill_count_row += 2f;
            }

            // draw all the platforms
            foreach (Platform platform in platforms)
            {
                platform.Draw(spriteBatch);
            }

            foreach (PowerUp powerup in powerups)
            {
                if (powerup.Body.IsDisposed == false)
                {
                    powerup.Draw(spriteBatch);
                } 
            }

            spriteBatch.End();

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
