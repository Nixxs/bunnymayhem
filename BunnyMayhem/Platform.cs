﻿using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BunnyMayhem
{
    class Platform
    {
        private Vector2 _origin;
        private Texture2D _texture;
        private Body _body;
        private int _height;
        private int _width;

        public Platform(GraphicsDevice screen, World world, Texture2D texture, int width, int height, Color color, Vector2 position, float units_ratio, BodyData.Type type)
        {
            ConvertUnits.SetDisplayUnitToSimUnitRatio(units_ratio);
            _height = height;
            _width = width;

            float restitution;
            if (type == BodyData.Type.FLOOR)
            {
                restitution = 0.0f;
            } else
            {
                restitution = 0.3f;
            }

            // define the orgin as the centre of the shape
            _origin = new Vector2(width / 2f, height / 2f);

            // create the texture which defines how this object looks
            _texture = texture;

            // create the body and assign it some values
            _body = BodyFactory.CreateRectangle
            (
                world, 
                ConvertUnits.ToSimUnits(width), 
                ConvertUnits.ToSimUnits(height), 
                1.0f, 
                new Vector2(ConvertUnits.ToSimUnits(position.X), ConvertUnits.ToSimUnits(position.Y))
            );
            _body.IsStatic = true;
            _body.Restitution = restitution;
            _body.Friction = 0.1f;
            _body.UserData = new BodyData(type, this);
        }

        public Body Body
        {
            get { return _body; }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            // draws the grass block texture on horizontal platforms
            // i think i need to make the platforms tiled blocks to make maps more flexible
            // otherwise we'll need to just have a few differnt sized textures to use with each platform
            int col = 0;
            while ((col * _texture.Width) + _texture.Width < _width)
            {
                spriteBatch.Draw(_texture, ConvertUnits.ToDisplayUnits(_body.Position) + new Vector2(_texture.Width * col, -7), new Rectangle(0, 0, _texture.Width, 30), Color.White, 0.0f, _origin, 1.0f, SpriteEffects.None, 0.0f);
                col += 1;
            }
            spriteBatch.Draw(_texture, ConvertUnits.ToDisplayUnits(_body.Position) + new Vector2(_texture.Width * col, -7), new Rectangle(0, 0, _width - (_texture.Width * col), 30), Color.White, 0.0f, _origin, 1.0f, SpriteEffects.None, 0.0f);
        }
    }
}
