﻿using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;

namespace BunnyMayhem
{
    class Player
    {
        private Vector2 _origin;
        private Texture2D _texture;
        private Body _body;
        private Color _color;
        private Vector2 _start_position;
        private World _world;

        private KeyboardState key_state;

        private Rectangle _animation_frame;
        private float _animation_speed;
        private float _animation_timer;
        private int _frame_width;
        private int _frame_height;
        private int _animation_row;
        private int _animation_col;
        private action _animation_action;
        private SpriteEffects _effect;

        private Keys _jump_control;
        private Keys _left_control;
        private Keys _right_control;
        private Keys _stomp_control;
        private bool _is_midair;
        private bool _can_stomp;
        private bool _is_stomping;
        private float _stomp_cd;
        private float _stomp_timer;
        private bool _can_jump;

        private float _scale;
        private float _speed;
        private float _max_speed;
        private float _jump_speed;
        private float _stomp_speed;
        private float _horizontal_dampening;
        private float _downward_gravity;
        private float _upward_gravity;

        private bool _is_dead;
        private float _death_timer;
        private List<Body> _bunny_corpses;
        private Vector2 _death_position;
        private Texture2D _corpse_texture;
        private int _max_blood;
        private int _blood_count;
        private int _blood_cap;

        private int _kills;

        private List<SoundEffect> _sound_effects;
        private bool _played_death_sound;

        private Random _random;
        private float _powerup_timer;
        private PowerUp _active_powerup;
        private bool _has_powerup;

        public Player(GraphicsDevice screen, List<SoundEffect> sound_effects, World world, Texture2D texture, int frame_width, int frame_height, Vector2 position, Color color, float units_ratio, Keys left_control, Keys right_control, Keys jump_control, Keys stomp_control)
        {
            ConvertUnits.SetDisplayUnitToSimUnitRatio(units_ratio);
            _world = world;

            _sound_effects = sound_effects;
            _played_death_sound = false;

            _left_control = left_control;
            _right_control = right_control;
            _jump_control = jump_control;
            _stomp_control = stomp_control;
            _is_midair = true;
            _can_jump = false;
            _can_stomp = true;
            _is_stomping = false;

            _random = new Random();

            _blood_count = 0;//keeps track of the blood being spawned gets reset after player comes alive again
            _max_blood = 25;// the amount of blood that gets spawned on each kill
            _blood_cap = 75; // the amount of blood that can be on the screen at once time
            _is_dead = false;
            _death_timer = 0f;
            _bunny_corpses = new List<Body>();
            _corpse_texture = new Texture2D(screen, 5, 5);
            Color[] corpse_fill = new Color[5 * 5];
            for (int i=0; i < (5*5); i++)
            {
                corpse_fill[i] = Color.Red;
            }
            _corpse_texture.SetData<Color>(corpse_fill);

            _kills = 0;

            // movement tuning this affects how the player feels
            _scale = 1.25f;
            _speed = 2f * _scale;
            _max_speed = 12f * _scale;
            _jump_speed = 6f * _scale;
            _stomp_speed = 8f * _scale;
            _stomp_cd = 0.5f;
            _horizontal_dampening = 7.0f * _scale;
            _downward_gravity = 3.0f * _scale;
            _upward_gravity = 4.0f * _scale;

            _animation_row = 0;
            _animation_col = 0;
            _animation_timer = 0;
            _animation_speed = 280f;
            _frame_width = frame_width;
            _frame_height = frame_height;
            _animation_frame = new Rectangle(0, 0, _frame_width, _frame_height);
            _animation_action = action.STANDING;

            _color = color;
            _texture = texture;
            _origin = new Vector2(frame_width / 2f, frame_height / 2f);
            _start_position = ConvertUnits.ToSimUnits(position.X, position.Y);
            _body = BodyFactory.CreateCircle
            (
                _world, 
                ConvertUnits.ToSimUnits(((_frame_width/2) * _scale) - 5),
                1.0f,
                _start_position
            );
            _body.BodyType = BodyType.Dynamic;
            _body.Restitution = 0.0f;
            _body.Friction = 0.5f;
            _body.UserData = new BodyData(BodyData.Type.PLAYER, this);

            _body.OnCollision += _body_OnCollision;
            _body.OnSeparation += _body_OnSeparation;
        }

        private void _body_OnSeparation(Fixture player_body, Fixture collision_body)
        {
            BodyData.Type collision_body_type = (collision_body.Body.UserData as BodyData).SpriteType;

            if (collision_body_type == BodyData.Type.FLOOR)
            {
                // reset the flags when player separates from a floor or player
                _is_midair = true;
                _can_jump = false;
                _body.LinearDamping = 0.0f;
            }

        }

        private bool _body_OnCollision(Fixture player_body, Fixture collision_body, Contact contact)
        {
            BodyData.Type collision_body_type = (collision_body.Body.UserData as BodyData).SpriteType;

            if (collision_body_type == BodyData.Type.POWERUP)
            {
                _active_powerup = ((collision_body.Body.UserData as BodyData).ParentObject as PowerUp);
                _active_powerup.ApplyPower(this);
                _has_powerup = true;
                _powerup_timer = _active_powerup.Timer;
                collision_body.Body.Dispose();
            }

            if (collision_body_type == BodyData.Type.FLOOR)
            {
               if ((_body.Position.Y < collision_body.Body.Position.Y) && contact.IsTouching)
                {
                    // set the flags that need to be set while player is in contact with and is above a floor or player
                    _is_midair = false;
                    _can_jump = true;
                    _body.LinearDamping = _horizontal_dampening;
                }
            }

            // when a player hits another player object 
            if (collision_body_type == BodyData.Type.PLAYER)
            {
                // if the player is higher than the other player
                if (_body.Position.Y < (collision_body.Body.Position.Y + ConvertUnits.ToSimUnits(3)))
                {
                    // The player was higher than the other player during the collission so he is safe
                    // if the player has settled is vertical velocity and hopefully is no longer mid air he can jump
                }
                else
                {
                    // the player got hit by another player from above and the other player was stomping then this player dies and the other player gets a kill
                    if (((collision_body.Body.UserData as BodyData).ParentObject as Player).IsStomping == true)
                    {
                        _is_dead = true;
                        _death_position = _body.Position;
                        // increment the killing players kill count by 1
                        ((collision_body.Body.UserData as BodyData).ParentObject as Player).Kills += 1;
                    }
                }
            }

            return true;
        }

        public void Update(GameTime gameTime)
        {
            if (_powerup_timer > 0)
            {
                _powerup_timer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                if (_powerup_timer <= 0)
                {
                    _active_powerup.RevertPower(this);
                }
            }

            // clean up old blood
            while (_bunny_corpses.Count > _blood_cap)
            {
                _bunny_corpses[0].Dispose();
                _bunny_corpses.RemoveAt(0);
            }

            // check if the player is dead or not
            if (_is_dead)
            {
                // here we can do what ever needs to be done to the player when he dies
                if (_played_death_sound == false)
                {
                    _sound_effects[0].Play();
                    _sound_effects[3].Play();
                    _played_death_sound = true;
                }
                
                // here is where we can spawn or move the death textures for the player
                if (_blood_count < _max_blood)
                {
                    _blood_count += 1;
                    Body _bunny_corpse = BodyFactory.CreateCircle
                    (
                        _world,
                        ConvertUnits.ToSimUnits(2.5f),
                        1.0f,
                        _death_position
                    );
                    _bunny_corpse.BodyType = BodyType.Dynamic;
                    _bunny_corpse.Restitution = 0.4f;
                    _bunny_corpse.Friction = 0.01f;
                    _bunny_corpse.UserData = new BodyData(BodyData.Type.CORPSE, this);
                    _bunny_corpse.ApplyLinearImpulse(new Vector2(_random.Next(-3, 3) * 0.01f, _random.Next(-3, 3) * 0.01f));
                    _bunny_corpses.Add(_bunny_corpse);
                }

               
                // teleport the player off somewhere and reset his movement vectors to 0
                // the player will stay there until is_dead is false
                _body.Position = _start_position;
                _body.ResetDynamics();

                // put the player back into the game after 3s
                if (_death_timer >= 0.3f)
                {
                    _is_dead = false;
                    _played_death_sound = false;
                    _blood_count = 0;
                    _death_timer = 0;
                }
                else
                {
                    _death_timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                }
            }
            else
            {
                UpdateMovement(gameTime);
            }

            _animation_timer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            if (_animation_timer > _animation_speed)
            {
                UpdateAnimation();
            }
            
        }

        private enum action
        {
            STANDING,
            MOVING,
            JUMPING,
        }

        private void UpdateAnimation()
        {
            _animation_col += 1;

            switch (_animation_action)
            {
                case action.STANDING:
                    _animation_row = 0;
                    _animation_speed = 280f;
                    if (_animation_col > 5)
                    {
                        _animation_col = 0;
                    }
                    break;
                case action.MOVING:
                    _animation_row = 1;
                    _animation_speed = 150f;
                    if (_animation_col > 2)
                    {
                        _animation_col = 0;
                    }
                    break;

                case action.JUMPING:
                    _animation_row = 2;
                    _animation_speed = 100f;
                    if (_animation_col > 0)
                    {
                        _animation_col = 0;
                    }
                    break;
            }

            int animation_x = _animation_col * _frame_width;
            int animation_y = _animation_row * _frame_height;
            _animation_frame = new Rectangle(animation_x, animation_y, _frame_width, _frame_height);
            _animation_timer = 0;
        }

        private void UpdateMovement(GameTime gameTime)
        {
            key_state = Keyboard.GetState();
            _animation_action = action.STANDING;

            // horizontal movement while in the air
            if (_is_midair)
            {
                _animation_action = action.JUMPING;

                // linear dampening resets to 0 when the player is in the air and set back to 5 when it touches a surface
                _body.LinearDamping = 0.0f;

                // gravity tuning 
                if (_body.LinearVelocity.Y < 0) //player is moving up
                {
                    _body.GravityScale = _upward_gravity;
                }
                else if (_body.LinearVelocity.Y > 0) //player is moving down
                {
                    _body.GravityScale = _downward_gravity;
                }
            }

            if (_is_midair == false)
            {
                // apply linear dampening if the player isnt pressing any horizontal controls and is on the ground
                _body.LinearDamping = _horizontal_dampening;
            }

            if (key_state.IsKeyDown(_left_control))
            {
                //_body.LinearVelocity -= _turning_speed;
                //_body.ApplyLinearImpulse(new Vector2(-_resulting_speed, 0));
                if (_is_midair == false)
                {
                    _body.LinearVelocity -= new Vector2(_speed, 0);
                }
                else
                {
                    _body.LinearVelocity -= new Vector2(_speed/4, 0);
                }
                _effect = SpriteEffects.None;
                _animation_action = action.MOVING;
            }
            if (key_state.IsKeyDown(_right_control))
            {
                //_body.LinearVelocity += _turning_speed;
                //_body.ApplyLinearImpulse(new Vector2(_resulting_speed, 0));
                if (_is_midair == false)
                {
                    _body.LinearVelocity += new Vector2(_speed, 0);
                }
                else
                {
                    _body.LinearVelocity += new Vector2(_speed/4, 0);
                }
                _effect = SpriteEffects.FlipHorizontally;
                _animation_action = action.MOVING;
            }
            if (key_state.IsKeyDown(_jump_control) && _is_midair == false && _can_jump == true)
            {
                _can_jump = false;
                _body.ApplyLinearImpulse(new Vector2(0, -_jump_speed));
                _animation_action = action.JUMPING;
                _sound_effects[1].Play();
            }

            if (_can_stomp == false)
            {
                if (_stomp_timer < _stomp_cd)
                {
                    _stomp_timer += (float)gameTime.ElapsedGameTime.TotalSeconds;
                }
                else
                {
                    _can_stomp = true;
                    _is_stomping = false;
                    _stomp_timer = 0;
                }
            }

            //if (key_state.IsKeyUp(_stomp_control))
            //{
            //    _is_stomping = false;
            //}

            if (key_state.IsKeyDown(_stomp_control) && _is_midair == true && _can_stomp == true)
            {
                _body.ApplyLinearImpulse(new Vector2(0, _stomp_speed));
                _is_stomping = true;
                _can_stomp = false;
                _animation_action = action.JUMPING;
                _sound_effects[2].Play();
            }

            // cap horizontal velocity
            if (_body.LinearVelocity.X < -_max_speed)
            {
                _body.LinearVelocity = new Vector2(-_max_speed, _body.LinearVelocity.Y);
            }
            if (_body.LinearVelocity.X > _max_speed)
            {
                _body.LinearVelocity = new Vector2(_max_speed, _body.LinearVelocity.Y);
            }
        }

        public bool IsStomping
        {
            get { return _is_stomping; }
        }

        public bool CanJump
        {
            get { return _can_jump; }
        }

        public struct Information
        {
            public int Kills;
            public Texture2D Icon;
            public Rectangle IconSourceRect;
            public Color IconColor;

            public Information(int kills, Texture2D icon, Rectangle icon_source_rect, Color color)
            {
                Kills = kills;
                Icon = icon;
                IconSourceRect = icon_source_rect;
                IconColor = color;
            }
        }

        public Information Info
        {
            get
            {
                return new Information(_kills, _texture, new Rectangle(0, 0, _frame_width, _frame_height), _color);
            }
        }

        public int Kills
        {
            get { return _kills; }
            set { _kills = value; }
        }

        public float Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }

        public float MaxSpeed
        {
            get { return _max_speed; }
            set { _max_speed = value; }
        }

        public void Draw(SpriteBatch spriteBatch)
        {

            // draw the player
            spriteBatch.Draw
            (
                _texture,
                ConvertUnits.ToDisplayUnits(_body.Position),
                _animation_frame,
                _color,
                0.0f,
                _origin,
                _scale,
                _effect,
                0.0f
            );

            // draw all the blood
            try
            {
                foreach (Body blood in _bunny_corpses)
                {
                    spriteBatch.Draw
                        (
                            _corpse_texture,
                            ConvertUnits.ToDisplayUnits(blood.Position),
                            null,
                            Color.White,
                            blood.Rotation,
                            new Vector2(2.5f, 2.5f),
                            1.0f,
                            SpriteEffects.None,
                            0.0f
                        );
                }
            }
            catch (Exception)
            {

            }
            
        }
    }
}
