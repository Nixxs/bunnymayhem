﻿using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;
using System.Collections.Generic;
namespace BunnyMayhem
{
    class PowerUp
    {
        private Texture2D _texture;
        private Body _body;
        private Vector2 _origin;
        private float previous_speed;
        private float previous_max_speed;
        private float new_max_speed;
        private float new_speed;
        private float _timer;

        public PowerUp(World world, Texture2D texture, Vector2 position, float units_ratio)
        {
            ConvertUnits.SetDisplayUnitToSimUnitRatio(units_ratio);
            _texture = texture;
            _origin = new Vector2(_texture.Width/2, _texture.Height/2);

            _body = BodyFactory.CreateRectangle
            (
                world,
                ConvertUnits.ToSimUnits(_texture.Width),
                ConvertUnits.ToSimUnits(_texture.Height),
                1.0f,
                new Vector2(ConvertUnits.ToSimUnits(position.X), ConvertUnits.ToSimUnits(position.Y))
            );
            _body.BodyType = BodyType.Dynamic;
            _body.Restitution = 0.3f;
            _body.Friction = 0.5f;
            _body.UserData = new BodyData(BodyData.Type.POWERUP, this);

            _timer = 10000f;
        }

        public void ApplyPower(Player player)
        {
            previous_speed = player.Speed;
            previous_max_speed = player.MaxSpeed;
            new_speed = player.Speed * 2;
            new_max_speed = player.MaxSpeed * 2;

            player.Speed = new_speed;
            player.MaxSpeed = new_max_speed;
        }

        public void RevertPower(Player player)
        {
            player.Speed = previous_speed;
            player.MaxSpeed = previous_max_speed;
        }

        public float Timer
        {
            get { return _timer; }
        }

        public Body Body
        {
            get { return _body; }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_texture, ConvertUnits.ToDisplayUnits(_body.Position), new Rectangle(0, 0, _texture.Width, _texture.Height), Color.White, 0.0f, _origin, 1.0f, SpriteEffects.None, 1.0f);
        }
    }
}
